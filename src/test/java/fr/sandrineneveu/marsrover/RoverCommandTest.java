package fr.sandrineneveu.marsrover;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

public class RoverCommandTest {
	
	@Test
	void shouldRoverTurnLeftWhenReceiveCommandL(){
	    //GIVEN
		Planet planet = getPlanet();
	    Rover rover = getRoverBuilder().x(1).y(1).build();
	    String command = "L";
	    
	    //WHEN
	    rover.landing(planet, command);
	    
	    //THEN
	    assertEquals(Direction.W, rover.getDirection());
	    assertEquals(1, rover.getX());
	    assertEquals(1, rover.getY());
	}
	
	@Test
	void shouldRoverTurnLeftWhenReceiveCommandl(){
	    //GIVEN
		Planet planet = getPlanet();
	    Rover rover = getRoverBuilder().x(1).y(1).build();
	    String command = "l";
	    
	    //WHEN
	    rover.landing(planet, command);
	    
	    //THEN
	    assertEquals(Direction.W, rover.getDirection());
	    assertEquals(1, rover.getX());
	    assertEquals(1, rover.getY());
	}

	@Test
	void shouldRoverTurnRightWhenReceiveCommandR(){
	    //GIVEN
		Planet planet = getPlanet();
	    Rover rover = getRoverBuilder().x(1).y(1).build();
	    String command = "R";
	    
	    //WHEN
	    rover.landing(planet, command);
	    
	    //THEN
	    assertEquals(Direction.E, rover.getDirection());
	    assertEquals(1, rover.getX());
	    assertEquals(1, rover.getY());
	}
	
	@Test
	void shouldRoverTurnRightWhenReceiveCommandr(){
	    //GIVEN
		Planet planet = getPlanet();
	    Rover rover = getRoverBuilder().x(1).y(1).build();
	    String command = "r";
	    
	    //WHEN
	    rover.landing(planet, command);
	    
	    //THEN
	    assertEquals(Direction.E, rover.getDirection());
	    assertEquals(1, rover.getX());
	    assertEquals(1, rover.getY());
	}
	
	@Test
	void shouldRoveMoveForwardWhenReceiveCommandF(){
	    //GIVEN
		Planet planet = getPlanet();
	    Rover rover = getRoverBuilder().x(1).y(1).build();
	    String command = "F";
	    
	    //WHEN
	    rover.landing(planet, command);
	    
	    //THEN
	    assertEquals(Direction.N, rover.getDirection());
	    assertEquals(1, rover.getX());
	    assertEquals(2, rover.getY());
	}

	@Test
	void shouldRoveMoveForwardWhenReceiveCommandf(){
	    //GIVEN
		Planet planet = getPlanet();
	    Rover rover = getRoverBuilder().x(1).y(1).build();
	    String command = "f";
	    
	    //WHEN
	    rover.landing(planet, command);
	    
	    //THEN
	    assertEquals(Direction.N, rover.getDirection());
	    assertEquals(1, rover.getX());
	    assertEquals(2, rover.getY());
	}

	@Test
	void shouldRoverMoveBackwardWhenReceiveCommandB(){
	    //GIVEN
		Planet planet = getPlanet();
	    Rover rover = getRoverBuilder().x(1).y(1).build();
	    String command = "B";
	    
	    //WHEN
	    rover.landing(planet, command);
	    
	    //THEN
	    assertEquals(Direction.N, rover.getDirection());
	    assertEquals(1, rover.getX());
	    assertEquals(5, rover.getY());
	}

	@Test
	void shouldRoverMoveBackwardWhenReceiveCommandb(){
	    //GIVEN
		Planet planet = getPlanet();
	    Rover rover = getRoverBuilder().x(1).y(1).build();
	    String command = "b";
	    
	    //WHEN
	    rover.landing(planet, command);
	    
	    //THEN
	    assertEquals(Direction.N, rover.getDirection());
	    assertEquals(1, rover.getX());
	    assertEquals(5, rover.getY());
	}
	
	@Test
	void shouldRoverMoveForwardWhenReceiveCommandFAndPositionIsMaxOfThePlanet(){
	    //GIVEN
		Planet planet = getPlanet();
	    Rover rover = getRoverBuilder().x(1).y(5).build();
	    String command = "F";
	    
	    //WHEN
	    rover.landing(planet, command);
	    
	    //THEN
	    assertEquals(Direction.N, rover.getDirection());
	    assertEquals(1, rover.getX());
	    assertEquals(1, rover.getY());
	}

	@Test
	void shouldErrorWhenRoverReceiveUnknownSimpleCommand(){
	    //GIVEN
		Planet planet = getPlanet();
	    Rover rover = getRoverBuilder().x(1).y(1).build();
	    String command = "x";
	    
	    //WHEN & THEN
	    Throwable exception = assertThrows(RoverException.class, () -> rover.landing(planet, command));
	    assertEquals("unknown command", exception.getMessage());
	}
	
	private Rover.RoverBuilder getRoverBuilder() {
		return Rover.builder().direction(Direction.N);
	}
	
	private Planet getPlanet(){
	    return Planet.builder().xDimension(5).yDimension(5).build();
	}
}
