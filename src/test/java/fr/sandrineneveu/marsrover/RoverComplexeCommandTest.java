package fr.sandrineneveu.marsrover;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

public class RoverComplexeCommandTest {
	
	@Test
	void shouldErrorWhenRoverReceiveUnknownComplexeCommand(){
	    //GIVEN
	    Planet planet = getPlanet().build();
	    Rover rover = getRoverBuilder().x(1).y(1).build();
	    String command = "F,F,R,x,F,L";
	    
	    //WHEN & THEN
	    Throwable exception = assertThrows(RoverException.class, () -> rover.landing(planet, command));
	    assertEquals("unknown command", exception.getMessage());
	    assertEquals(Direction.E, rover.getDirection());
	    assertEquals(1, rover.getX());
	    assertEquals(3, rover.getY());
	}

	@Test
	void shouldRoverMoveWithComplexeCommand(){
	    //GIVEN
	    Planet planet = getPlanet().build();
	    Rover rover = getRoverBuilder().x(1).y(1).build();
	    String command = "F,F,R,F,L,B,B,B,L,B";
	    
	    //WHEN
	    rover.landing(planet, command);
	    
	    //THEN
	    assertEquals(Direction.W, rover.getDirection());
	    assertEquals(3, rover.getX());
	    assertEquals(5, rover.getY());
	}
	
	@Test
	void shouldRoverStopSequenceCommandWhenObstacleIsPresent(){
	    //GIVEN
		List<Coordinate> obstaclesList = new ArrayList<Coordinate>();
		obstaclesList.add(Coordinate.builder().x(2).y(3).build());
	    Planet planet = getPlanet().obstacles(obstaclesList).build();
	    Rover rover = getRoverBuilder().x(1).y(1).build();
	    String command = "F,F,R,F,L,B,B,B,L,B";
	    
	    //WHEN & THEN	   
	    assertThrows(RoverException.class, () -> rover.landing(planet, command));
	    assertEquals(Direction.E, rover.getDirection());
	    assertEquals(1, rover.getX());
	    assertEquals(3, rover.getY());
	}

	private Rover.RoverBuilder getRoverBuilder() {
		return Rover.builder().direction(Direction.N);
	}

	private Planet.PlanetBuilder getPlanet(){
	    return Planet.builder().xDimension(5).yDimension(5);
	}
}
