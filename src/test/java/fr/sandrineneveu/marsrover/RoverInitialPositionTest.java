package fr.sandrineneveu.marsrover;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

public class RoverInitialPositionTest {

	@Test
	void createRoverWithInitialPosition() {
		Rover rover = getRoverBuilder().x(1).y(4).build();
		assertEquals(1, rover.getX());
		assertEquals(4, rover.getY());
		assertEquals(Direction.N, rover.getDirection());
	}
	
	@Test
	void shouldErrorWhenRoverInitialPositionIsWithoutplanet(){
	    //GIVEN
	    Planet planet = getPlanet();
	    Rover rover = getRoverBuilder().x(6).y(6).build();
	    
	    //WHEN & THEN
	    Throwable exception = assertThrows(RoverException.class, () -> rover.landing(planet, ""));
	    assertEquals("the rover is off the planet", exception.getMessage());
	}

	@Test
	void shouldErrorWhenCreateRoverWithoutplanet(){
		//GIVEN
		Rover rover = getRoverBuilder().x(1).y(1).build();
		
	    //WHEN & THEN
	    Throwable exception = assertThrows(RoverException.class, () -> rover.landing(null, ""));
	    assertEquals("it takes a planet to land", exception.getMessage());
	}
	
	@Test
	void shouldErrorWhenCreateRoverWithEmptyX() {
		//GIVEN
		Planet planet = getPlanet();
		Rover rover = getRoverBuilder().y(1).build();
		
		//WHEN & THEN
	    Throwable exception = assertThrows(RoverException.class, () -> rover.landing(planet, ""));
	    assertEquals("we need initial position to land the rover", exception.getMessage());
	}
	
	@Test
	void shouldErrorWhenCreateRoverWithEmptyY() {
		//GIVEN
		Planet planet = getPlanet();
		Rover rover = getRoverBuilder().x(1).build();
		
		//WHEN & THEN
	    Throwable exception = assertThrows(RoverException.class, () -> rover.landing(planet, ""));
	    assertEquals("we need initial position to land the rover", exception.getMessage());
	}
	
	@Test
	void shouldErrorWhenCreateRoverWithEmptyDirection() {
		//GIVEN
		Planet planet = getPlanet();
		Rover rover = getRoverBuilder().x(1).y(1).direction(null).build();
		
		//WHEN & THEN
	    Throwable exception = assertThrows(RoverException.class, () -> rover.landing(planet, ""));
	    assertEquals("we need initial position to land the rover", exception.getMessage());
	}

	private Rover.RoverBuilder getRoverBuilder() {
		return Rover.builder().direction(Direction.N);
	}

	private Planet getPlanet(){
	    return Planet.builder().xDimension(5).yDimension(5).build();
	}
}
