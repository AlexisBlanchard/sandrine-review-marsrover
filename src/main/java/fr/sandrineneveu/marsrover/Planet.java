package fr.sandrineneveu.marsrover;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
@AllArgsConstructor
public class Planet {

	Integer xDimension;
	Integer yDimension;
	List<Coordinate> obstacles;
}
