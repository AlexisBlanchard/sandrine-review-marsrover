package fr.sandrineneveu.marsrover;

public interface Command {

	void landing(Planet planet, String command);

}
