package fr.sandrineneveu.marsrover;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
@AllArgsConstructor
public class Coordinate {
	
	Integer x;
	Integer y;

}
