package fr.sandrineneveu.marsrover;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
@AllArgsConstructor
public class Rover implements Command{
	
	Integer x;
	Integer y;
	Integer maxX;
	Integer maxY;
	Direction direction;
	
	@Override
	public void landing(Planet planet, String command) {
		if(this.x == null || this.y == null|| this.direction == null) {
			throw new RoverException("we need initial position to land the rover");
		}
		if(planet == null) {
			throw new RoverException("it takes a planet to land");
		}else if(planet.getXDimension() < this.x || planet.getYDimension() < this.y) {
			throw new RoverException("the rover is off the planet");
		}else {
			this.setMaxX(planet.xDimension);
			this.setMaxY(planet.yDimension);
			this.receive(command, planet);
		}
	}

	private void receive(String command, Planet planet) {
		if(command == null || command.isEmpty()) {
			throw new RoverException("we need command for the rover");
		}else {
			List<String> commandsList = new ArrayList<>(Arrays.asList(command.toUpperCase().split(",")));
            for (String c : commandsList) {
                if(c.equals("R") || c.equals("L")){
                    turn(c);
                }else if(c.equals("F") || c.equals("B")){
                    go(c, planet);
                }else{
                    throw new RoverException("unknown command");
                }
            }
		}
	}

	private void turn(String command) {
		if(command.equals("L")){
            this.setDirection(this.direction.turnLeft());
        }else if(command.equals("R")){
            this.setDirection(this.direction.turnRight());
        }		
	}

	private void go(String command, Planet planet) {
		Integer nextCoordinate = 0;
		switch(this.direction){
	        case N:
	        	nextCoordinate = command.equals("F") ? this.y + 1 : this.y - 1;
	            break;
	        case E:
	        	nextCoordinate = command.equals("F") ? this.x + 1 : this.x - 1;
	            break;
	        case S:
	        	nextCoordinate = command.equals("F") ? this.y - 1 : this.y + 1;
	            break;
	        case W:
	        	nextCoordinate = command.equals("F") ? this.x - 1 : this.x + 1;
	            break;
		}
		if(hasObstacle(planet, nextCoordinate)){
			throw new RoverException("obstacle is present, rover stop to move");
		}else if(!positionIsGoodOnPlanet(nextCoordinate)) {
			this.move(nextCoordinate);
		}
	}
	
	private void move(Integer nextCoordinate) {
		if(this.direction.equals(Direction.N) || this.direction.equals(Direction.S)) {
			setY(nextCoordinate);
		}else if(this.direction.equals(Direction.E) || this.direction.equals(Direction.W)) {
			setX(nextCoordinate);
		}
	}
	
	private boolean positionIsGoodOnPlanet(Integer nextCoordinate){
		if(this.direction.equals(Direction.E) || this.direction.equals(Direction.W)) {
			if(nextCoordinate == 0) {
				setX(maxX);
				return true;
			}else if(nextCoordinate > this.maxX) {
				this.x = 1;
				return true;
			}
		}
		
		if(this.direction.equals(Direction.N) || this.direction.equals(Direction.S)) {
			if(nextCoordinate == 0) {
				setY(maxY);
				return true;
			}else if(nextCoordinate > this.maxY) {
				this.y = 1;
				return true;
			}
		}
		return false;
	}
	
	private boolean hasObstacle(Planet planet, Integer nextCoordinate) {
		if(planet.obstacles != null) {
			for(Coordinate obstable: planet.obstacles) {
				if((obstable.x == nextCoordinate && this.y == obstable.y) || (obstable.y == nextCoordinate && this.x == obstable.x) ) {
					return true;
				}
			}
		}
		return false;
	}

}
